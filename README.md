# winccoadeps-release

* This rpm defines .repo files for custom libraries that have been built to support the operation of WinCC OA 3.16 on CentOS8
* The repository currently contains the RPMs `compat-libicu50` and `compat-libwebp4`, which provide EL7 versions of these libraries without conflicting with EL8 versions (should they be installed)

