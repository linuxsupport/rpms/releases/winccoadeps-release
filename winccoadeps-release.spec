%define product winccoadeps
%define debug_package %{nil}

Name:		%{product}-release
Version:	1.0
Release:	1%{?dist}
Summary:	CERN %{product} repository release file
Group:		System Environment/Base
License:	GPLv2
URL:		http://linux.cern.ch/
BuildArch:  noarch
Source0:	%{product}-stable.repo
Source1:	%{product}-qa.repo
Source2:	%{product}-testing.repo

%description
The package contains yum configuration for the CERN %{product} repository

%prep
%setup -q  -c -T
install -pm 0644 %{SOURCE0} %{SOURCE1} %{SOURCE2} .

%build

%install
rm -rf $RPM_BUILD_ROOT
install -dm 0755 $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d
install -pm 0644 %{SOURCE0} %{SOURCE1} %{SOURCE2} $RPM_BUILD_ROOT%{_sysconfdir}/yum.repos.d

%files
%defattr(-,root,root,-)
%doc
%config(noreplace) /etc/yum.repos.d/*

%changelog
* Fri Mar 13 2020 Ben Morrice <ben.morrice@cern.ch> 1.0-1
- Initial release
